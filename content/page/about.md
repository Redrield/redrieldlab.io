---
title: About Me
comments: false
---

My name is Rhys, I'm a high school junior, avid programmer, and lead programmer for FRC Team 4069. Most of my work is done in Rust, and Kotlin.
